# Roadmap

This is a general roadmap of features to be
added into Morphism in the near future.

## Status

- Current Version: pre0.0.1

## Planned Features

Undefined.

## Planned Versions

### pre0.0.1

- Basic entity system
- Palette and shadermap compilers
- Simple bitmap compression and decompression
- Sprite (fg*bg color layer, RGB (XYCol) normal & collision data layer) compiler

### pre0.0.2