import utilbase
import re
import struct
import sys


class Palette(object):
    def __init__(self, fn):
        if isinstance(fn, str):
            with open(fn, 'rb') as fp:
                ba = bytearray(fp.read())
                self.palette = [struct.unpack('3B', ba[i:i + 3]) for i in range(0, min(768, len(ba) - 3), 3)]

        else:
            self.palette = list(fn)
            self.palette = self.palette[:256]

    def __iter__(self):
        return self.get_colors()

    def set_color(self, i, color):
        self.palette[i] = tuple(color[:3]) + tuple(0 for i in range(max(0, 3 - len(color))))

    def get_color(self, i):
        return self.palette[i]

    def export_bytes(self):
        res = bytearray()

        for c in self.palette:
            res += struct.pack('3B', *c)

        return res

    def get_colors(self):
        return enumerate(iter(self.palette))

    def __len__(self):
        return len(self.palette)

def parse_color(c):
    if not c:
        return (0, 0, 0)

    elif c[:2] == '$#':
        ch = c[2:]
        ch = ch.split('#')[0]

        if len(ch) == 3:
            och = ch
            ch = ''

            for cc in och:
                ch += cc * 2

        r = ch[0:2]
        g = ch[2:4]
        b = ch[4:6]

        return (int(r, 16), int(g, 16), int(b, 16))

    else:
        c = c.split('#')[0]

        if re.match(r'rgb\(.+\)', c):
            c = re.sub(r'\s+', '')
            cf = re.match(r'rgb\((.+)\)', c).group(1)
            return tuple(int(comp) for comp in cf.split(','))[:3]

        else:
            raise ValueError(f'Color not recognized: {repr(c)}')

class PaletteCompiler(utilbase.Utility):
    name = 'Pal² - The Palette Pal'
    description = [
        'Compiles a text Morphism Palette file (.mpr)',
        'into a binary Morphism Palette file (.mpl).'
    ]

    def help_list(self):
        yield '- an input filename (extension is optional)'

    def main(self, in_fn, out_fn):
        with open(in_fn) as in_fp:
            pal = Palette([self.log_val('INFO', 'Adding color {}: {{0}}'.format(i), parse_color(l.strip())) for i, l in enumerate(iter(in_fp)) if re.split(r'(?<!\$)\#', l)[0].strip()])

        self.log('INFO', f'Read {len(pal.palette)} colors from {in_fn}')

        with open(out_fn, 'wb') as out_fp:
            data = pal.export_bytes()
            out_fp.write(data)
            self.log('FINISH', f'Wrote {len(data)} bytes to {out_fn}')
        
        return 0

    def run(self, argv):
        if len(argv) <= 1 or argv[1] in ('--help', '-h'):
            self.print_help()
            return int(len(sys.argv) > 1)

        else:
            arg = re.sub(r'\.mpr$', '', sys.argv[1])
            return self.main(arg + '.mpr', arg + '.mpl')

if __name__ == "__main__":
    PaletteCompiler(sys.argv)