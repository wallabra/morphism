import datetime
import traceback


class Utility(object):
    name = "Unnamed Withcery Utility (uwu) v667.69"
    description = [
        'Does nothing. Just like you. :3',
        '(seriously, thought, this is a base',
        'class, please subclass it instead...)'
    ]
    help_list_label = 'Arguments:'

    def exit(self, code):
        self.log_fp.close()
        exit(code)

    def __init__(self, argv = ()):
        self.print_title()

        with open(type(self).name + '.log', 'w') as self.log_fp:
            try:
                status = self.run(list(argv))

            except BaseException as err:
                self.log('EXCEPT', '{}: {}'.format(type(err).__name__, str(err)))
                
                for t in traceback.format_tb(err.__traceback__):
                    for l in t.split('\n'):
                        self.log('TRACE', l)

                self.exit(2)

            else:
                self.exit(status)

    def print_title(self):
        print()
        print('   > ' + type(self).name)
        print()

    def print_help(self, err_msg = None):
        if err_msg is not None:
            self.log('ERROR', err_msg)
            print()

        for l in type(self).description:
            print(l)
            
        print()
        print(type(self).help_list_label)

        for l in self.help_list():
            print('     ' + l)

        print()
        print()

    def log(self, kind, msg):
        log_data = '{} {} {}'.format(datetime.datetime.utcnow().strftime('%H:%M:%S').ljust(9), ('[' + kind + ']').ljust(9), msg)

        print(log_data)
        self.log_fp.write(log_data + '\n')

    def log_val(self, kind, msg, val):
        self.log(kind, msg.format(val))
        return val

    def run(self, argv):
        self.print_help()

    def help_list(self):
        yield 'No help available for this utility.'

if __name__ == '__main__':
    Utility()