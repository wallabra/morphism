import struct
import re
import sys
import palette
import utilbase
import collections


Shade = collections.namedtuple('Shade', ('normal', 'dark', 'bright'))


class ShadeMap(object):
    def __init__(self, fn):
        if isinstance(fn, str):
            with open(fn, 'rb') as fp:
                ba = bytearray(fp.read())
                self.shades = [struct.unpack('2B', ba[i:i + 2]) for i in range(0, max(512, len(ba)), 2)]

        else:
            self.shades = list(fn)
            self.shades = self.shades[:256]

    def dark_shade_of(self, i):
        return self.shades[i][0]

    def bright_shade_of(self, i):
        return self.shades[i][0]

    def shade_of(self, i):
        return Shade(i, *self.shades[i])

    def export_bytes(self):
        res = bytearray()

        for c in self.shades:
            res += struct.pack('2B', *c)

        return res

def parse_shade(i, txt):
    txt = re.split(r' +', txt)

    if len(txt) == 3:
        ti = txt.pop(1)

        try:
            assert int(ti) == i, ValueError(f'The value of the triplet\'s current index field, {ti}, is not equal to the actual index, {i}!')

        except ValueError:
            raise ValueError(f"Invalid (non-numerical) triplet current index field value: {repr(ti)} (expected '{i}')")

    try:
        return (i, *(int(x) for x in txt))

    except ValueError:
        raise ValueError(f'Invalid (non-numerical) index value found in #{i}: {", ".join(repr(x) for x in txt)}')

class ShademapCompiler(utilbase.Utility):
    name = 'ShadePal - The Shademap Pal'
    description = [
        'Compiles a text Morphism Shademap file (.msr)',
        'into a binary Morphism Shademap file (.msm).'
    ]

    def help_list(self):
        yield '- an input filename (extension is optional)'

    def main(self, in_fn, out_fn):
        with open(in_fn) as in_fp:
            shades = []
            shades = ShadeMap([self.log_val('INFO', 'Adding shademap entry to color {0} (dark: {{0[1]}}, bright: {{0[2]}})'.format(i), parse_shade(i, l.split('#')[0].strip()))[1:] for i, l in enumerate(iter(in_fp)) if l.split('#')[0].strip()])

        self.log('INFO', f'Read {len(shades.shades)} shademap entries from {in_fn}')

        with open(out_fn, 'wb') as out_fp:
            data = shades.export_bytes()
            out_fp.write(data)
            self.log('FINISH', f'Wrote {len(data)} bytes to {out_fn}')
        
        return 0

    def run(self, argv):
        if len(argv) <= 1 or argv[1] in ('--help', '-h'):
            self.print_help()
            return int(len(sys.argv) > 1)

        else:
            arg = re.sub(r'\.msr$', '', sys.argv[1])
            return self.main(arg + '.msr', arg + '.msm')

if __name__ == "__main__":
    ShademapCompiler(sys.argv)