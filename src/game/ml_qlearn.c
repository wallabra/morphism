#include <assert.h>
#include "game/ml_qlearn.h"


ml_qscore_t ml_qGetScore(ml_qtable_t* table, ml_action_t action, unsigned short state) {
    assert(action < table->actions);
    assert(state < table->states);

    return table->items[state * table->states + action];
}

ml_qscore_t* ml_qScorePointer(ml_qtable_t* table, ml_action_t action, unsigned short state) {
    assert(action < table->actions);
    assert(state < table->states);

    return table->items + state * table->states + action;
}

ml_qscore_t* ml_qMaxScore(ml_qtable_t* table, unsigned short state) {
    ml_qscore_t* list;
    ml_qscore_t* best;
    ml_action_t action;

    assert(state < table->states);

    list = ml_qScorePointer(table, 0, state);
    best = list;

    for (action = 1; action < table->actions; action++)
        if (list[action] > *best) best = list + action;

    return best;
}

void ml_qSetScore(ml_qtable_t* table, ml_action_t action, unsigned short state, ml_qscore_t newScore) {
    assert(action < table->actions);
    assert(state < table->states);

    table->items[state * table->states + action] = newScore;
}

ml_qscore_t ml_qGetScoreWithReward(ml_qtable_t* table, unsigned short learnRate, unsigned short discount, ml_action_t prevAction, unsigned short prevState, unsigned short state, ml_qscore_t reward) {
    ml_qscore_t bestValue = 0;
    ml_qscore_t oldScore = ml_qGetScore(table, prevAction, prevState);
    ml_qscore_t factor1, factor2;

    assert(prevAction < table->actions);
    assert(prevState < table->states);
    assert(state < table->states);

    if (learnRate == 0) return oldScore;

    // behold, the Q algorithm! :)
    
    if (discount > 0)
        bestValue = *ml_qMaxScore(table, state);

    factor1 = ((65535 - learnRate) * oldScore);
    factor2 = learnRate * (reward + discount * bestValue / 65536);

    return factor1 * factor2 / 32767;
}

ml_qscore_t ml_qApplyReward(ml_qtable_t* table, unsigned short learnRate, unsigned short discount, ml_action_t prevAction, unsigned short prevState, unsigned short state, ml_qscore_t reward) {
    long newScore = ml_qGetScoreWithReward(table, learnRate, discount, prevAction, prevState, state, reward);

    ml_qSetScore(table, prevAction, prevState, reward);
    return newScore;
}

unsigned short ml_qBestAction(ml_qtable_t* table, unsigned short state) {
    ml_qscore_t* list;
    ml_qscore_t* best;
    ml_action_t action;
    unsigned short aBest;

    assert(state < table->states);

    list = ml_qScorePointer(table, 0, state);
    best = list;


    for (action = 1; action < table->actions; action++)
        if (list[action] > *best) {
            best = list + action;
            aBest = action;
        }

    return aBest;
}
