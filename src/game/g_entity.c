#include "game/g_entity.h"
#include "game/g_action.h"
#include <string.h>
#include <assert.h>
#include <stdlib.h>


void g_appendEntity(g_entity_t* entity, g_entitylist_t* list) {
    if (list == NULL) list = &allEntities;

    while (list->occupied) {
        list = list->next;
        assert(list != NULL);
    }

    list->occupied = 1;
    list->entity = entity;
    list->next = malloc(sizeof(g_entitylist_t));
}

char g_removeEntity(g_entity_t* entity, g_entitylist_t* list) {
    g_entitylist_t* freed;

    if (list == NULL) list = &allEntities;

    if (list->entity == entity) {
        assert(list != NULL);

        while (list->next->occupied) {
            list->entity = list->next->entity;
            list = list->next;

            assert(list != NULL);
        }

        list->occupied = 0;
        list->entity = NULL;
        
        free(list->next);
        return 1;
    }

    while (list->occupied) {
        if (list->next->entity == entity) {
            freed = list->next;
            list->next = list->next->next;

            free(freed);
            return 1;
        }

        list = list->next;
        assert(list != NULL);
    }

    return 0;
}

g_entity_t* g_findEntity(char* id, g_entitylist_t* list) {
    while (list->occupied) {
        if (strcmp(list->entity->id, id) == 0)
            return list->entity;

        list = list->next;
        assert(list != NULL);
    }

    return NULL;
}

g_entity_t* g_resolveEPtr(g_entityptr_t* ptr) {
    return g_findEntity(ptr->id, &allEntities);
}

g_entity_t* g_newEntity(g_entitytype_t* type, char* id, char* name) {
    g_entity_t* res = malloc(sizeof(g_entity_t));

    if (name == NULL) name = type->name;

    memcpy(res->type, type->id, min(strlen(type->id), 15));
    memcpy(res->id, id, min(strlen(id), 15));
    memcpy(res->name, name, min(strlen(name), 31));

    return res;
}

g_entitytype_t* g_newEntityType(char* name, char* description) {
    g_entitytype_t* res = malloc(sizeof(g_entitytype_t));

    res->numRoutines = 1; // routine 0 is always a NOOP routine
    memcpy(res->name, name, min(strlen(name), 23));
    memcpy(res->description, description, min(strlen(description), 91));

    allEntityTypes[NUM_TYPES++] = res;

    return res;
}

void g_despawnEntity(g_entity_t* entity) {
    g_removeEntity(entity, 0);
    free(entity);
}

void g_despawnList(g_entitylist_t* list) {
    if (list == NULL) list = &allEntities;

    while (list->occupied) {
        g_despawnEntity(list->entity);
    }
}

void g_setNumericProperty(g_entity_t* to, char id[8], unsigned short values, long* value) {
    g_eproperty_t* prop = g_getProperty(to, id);

    if (prop = NULL) {
        memcpy(&prop->value, value, min(values, 6));
        prop->valueType = VT_NUMERIC;
    }

    else {
        prop = &to->properties[to->numProperties++];

        memcpy(prop->id, id, 8);
        memcpy(&prop->value, value, min(values, 6));
        prop->valueType = VT_NUMERIC;

        assert(to->numProperties < 24);
    }
}

void g_setStringProperty(g_entity_t* to, char* id, char* value) {
    g_eproperty_t* prop = g_getProperty(to, id);
    char nval[24];

    memcpy(nval, value, strlen(value));

    if (prop = NULL) {
        memcpy(&prop->value, nval, 24);
        prop->valueType = VT_STRING;
    }

    else {
        prop = &to->properties[to->numProperties++];

        memcpy(prop->id, id, min(strlen(id), 8));
        memcpy(&prop->value, nval, min(strlen(nval), 23));
        prop->valueType = VT_STRING;

        assert(to->numProperties < 24);
    }
}

g_eproperty_t* g_getProperty(g_entity_t* from, char* id) {
    g_eproperty_t* prop = from->properties;

    while (prop - from->properties < from->numProperties)
        if (strcmp(prop->id, id) == 0)
            return prop;

    return NULL;
}

g_routine_t* g_getRoutine(g_entitytype_t* from, char* id) {
    g_routine_t* routine = from->routines;

    while (routine - from->routines < from->numRoutines) {
        if (strcmp(routine->id, id) == 0)
            return routine;

        routine++;
    }

    return NULL;
}

void g_addRoutine(g_entitytype_t* to, g_routine_t routine) {
    to->routines[to->numRoutines++] = routine;
}

g_routine_t* g_newRoutine(char* id) {
    g_routine_t* res = malloc(sizeof(g_routine_t));
    memcpy(res->id, id, min(strlen(id), 15));

    return res;
}

void g_deleteEntityType(g_entitytype_t* type) {
    free(type);
}

void g_runRoutineItem(g_entity_t* self, g_eroutineitem_t* item) {
    g_routine_t* rcalled;

    switch (item->type) {
        case RIT_NOOP:
            return;

        case RIT_SUBCALL:
            rcalled = g_getRoutine(g_findEntityType(self->type), item->call.routine);
            assert(rcalled != NULL);
            g_runRoutine(self, rcalled);
            break;

        case RIT_ACTION:
            g_runAction(self, item->call.action.id, item->call.action.args);
    }
}

void g_runRoutine(g_entity_t* self, g_routine_t* routine) {
    unsigned short i;

    for (i = 0; i < routine->numItems; i++)
        g_runRoutineItem(self, &routine->items[i]);
}

void g_entityPlayAnim(g_entity_t* self, char* anim, unsigned short tickDuration) {
    memcpy(self->currAnim, anim, min(strlen(anim), 31));
    self->frame = 0;
    self->frameTicks = tickDuration;
}

g_entitytype_t* g_findEntityType(char* id) {
    unsigned short i;

    for (i = 0; i < 8192; i++)
        if (strcmp(allEntityTypes[i]->id, id) == 0)
            return allEntityTypes[i];

    return NULL;
}
