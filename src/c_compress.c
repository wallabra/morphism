#include "c_compress.h"
#include <stdlib.h>
#include <string.h>


char* binf(c_binary_t* data) {
    char* res = malloc(1 + data->length * 8);
    unsigned long i;

    for (i = 0; i < data->length; i++) {
        res[i * 8] = data->data[i] & 0x80 ? '1' : '0';
        res[i * 8 + 1] = data->data[i] & 0x40 ? '1' : '0';
        res[i * 8 + 2] = data->data[i] & 0x20 ? '1' : '0';
        res[i * 8 + 3] = data->data[i] & 0x10 ? '1' : '0';
        res[i * 8 + 4] = data->data[i] & 0x8 ? '1' : '0';
        res[i * 8 + 5] = data->data[i] & 0x4 ? '1' : '0';
        res[i * 8 + 6] = data->data[i] & 0x2 ? '1' : '0';
        res[i * 8 + 7] = data->data[i] & 0x1 ? '1' : '0';
    }

    return res;
}

c_huffmanlike_t* c_hufflike_encode(c_binary_t* data) {
    unsigned short frequencies[256];
    unsigned short dictLen, dataLen, dictOccup;
    unsigned short iBit;
    unsigned long i, fi;
    c_huffmanlike_t *res = malloc(sizeof(c_huffmanlike_t));
    unsigned char* dict;
    unsigned char* dictPos = malloc(256);
    unsigned char curByte, bitPos;
    unsigned long cur;

    dictLen = 0;
    dataLen = 0;
    dictOccup = 0;

    res->numItems = data->length;

    // collect frequencies    
    for (i = 0; i < 256; i++) {
        frequencies[i] = 0;
        dictPos[i] = 1;
    }

    for (i = 0; i < data->length; i++) {
        if (frequencies[data->data[i]]++ == 0) dictLen++;
    }

    dict = malloc(dictLen);
    res->dictLength = dictLen;
    
    // create and sort the dictionary by frequency
    for (fi = 0; fi < 256; fi++) if (frequencies[fi] > 0) {
        for (i = 0; i < dictOccup; i++) {
            if (frequencies[dict[i]] < frequencies[fi] && dict[i] != fi) {
                memmove(dict + i + 1, dict + i, dictOccup);
                dict[i] = fi;
                dictOccup++;
                break;
            }
        }

        if (i == dictOccup) {
            dict[dictOccup++] = fi;
        }
    }

    for (i = 0; i < 256; i++)
        dictPos[dict[i]] = i;

    res->dict = dict;

    // compute result length
    for (i = 0; i < data->length; i++)
        dataLen += dictPos[data->data[i]] + 1;

    // encode the data with the dictionary
    res->dataLength = (dataLen + 7) / 8;
    res->data = malloc(res->dataLength);
    cur = 0;

    for (i = 0; i < data->length; i++) {
        if (dictPos[data->data[i]] == 0) {
            if (++bitPos >= 8) {
                res->data[cur++] = curByte;
                bitPos = 0;
                curByte = 0;
            }
        }

        else {
            for (iBit = dictPos[data->data[i]] + 1; iBit > 0; --iBit) {
                if (iBit - 1 > 0) curByte |= (1 << (7 - bitPos));
                if (++bitPos >= 8) {
                    res->data[cur++] = curByte;
                    bitPos = 0;
                    curByte = 0;
                }
            }
        }
    }

    free(dictPos);
    return res;
}

c_binary_t* c_hufflike_decode(c_huffmanlike_t* compressed) {
    unsigned long curItem = 0;
    unsigned char curByte = 0;
    unsigned char curBit = 0;
    unsigned char* resPtr;
    unsigned char itemAcc = 0;
    
    c_binary_t* res = malloc(sizeof(c_binary_t));
    res->data = malloc(compressed->numItems);
    res->length = compressed->numItems;

    resPtr = res->data;

    while (curItem < compressed->numItems) {
        if ((compressed->data[curByte] >> (7 - curBit)) & 1)
            itemAcc++;

        else {
            *(resPtr++) = compressed->dict[itemAcc];
            itemAcc = 0;
            curItem++;
        }

        curBit++;

        if (curBit > 7) {
            curBit = 0;
            curByte++;
        }
    }

    return res;
}

void c_binary_dealloc(c_binary_t* data) {
    free(data->data);
    free(data);
}

void c_hufflike_dealloc(c_huffmanlike_t* data) {
    free(data->data);
    free(data->dict);
    free(data);
}
