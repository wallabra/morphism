#ifndef HEADER_D_TEX
#define HEADER_D_TEX


#define BITMAP_ENC_NONE      0      // No encoding
#define BITMAP_ENC_HUFFLIKE  2      // Huffman-like frequency encoding

#define BITMAP_TYPE_PAL      0      // Paletted bitmap
#define BITMAP_TYPE_AXYZ     1      // Alpha + three-channel (e.g. RGB) 2-byte-per-pixel bitmap


typedef struct {
    unsigned char axyz:1;
    unsigned char hufflike:1;
} d_bitmaptype_t;

typedef struct {
    unsigned short width;
    unsigned short height;
    d_bitmaptype_t type;
} d_bitmapheader_t;

typedef struct {
    // A pixel is a 8-bit palette index (0 is transparent, so 255 colors max)
    d_bitmapheader_t header;
    unsigned short length;
    unsigned char* data;
} d_bitmap_pal_t;

typedef struct {
    // A pixel is an unsigned short (16 bits), with the following format:
    //    A XXXXX YYYYY ZZZZZ
    // Usually, a sprite data layer includes XY, collision, and lighting
    // factor:
    //    A XXXXX YYYYY CFFFF
    d_bitmapheader_t header;
    unsigned short* data;
} d_bitmap_axyz_t;

typedef union {
    d_bitmap_pal_t pal;
    d_bitmap_axyz_t axyz;
} d_bitmap_t;


d_bitmap_t *loadMemBitmap(unsigned short size, unsigned char* data);


#endif