#ifndef HEADER_D_VGA
#define HEADER_D_VGA

#include "display/d_common.h"


//extern unsigned char VGA_ENABLED = 0;

#define VGA                     ((unsigned char*)(0xA000 << 16))

#define VGA_PALETTE_INDEX       0x03c8
#define VGA_PALETTE_DATA        0x03c9

#define VGA_MODE_200P_256C      0x13
#define VGA_MODE_TEXT           0x00


void g_vga_setMode(unsigned char mode);
void g_vga_plotPixel(unsigned short x, unsigned short y, unsigned char color);
void g_vga_enable();
void g_vga_disable();
void g_vga_setPalette(g_palette_t* palette);


#endif
