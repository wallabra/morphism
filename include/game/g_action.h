#ifndef HEADER_G_ACTION
#define HEADER_G_ACTION

#include "c_hash.h"
#include "game/g_entity.h"


#define ACTHASH_DESPAWN               0xB86D6F85 // strhash("E_Despawn");
#define ACTHASH_SETPROPERTYSTR        0xB598C58F // strhash("E_SetPropertyStr");
#define ACTHASH_SETPROPERTYNUM        0xB598F86C // strhash("E_SetPropertyNum");
#define ACTHASH_SETPROPERTYNUMS       0x68B8059F // strhash("E_SetPropertyNums");
#define ACTHASH_PLAYANIM              0x58D25271 // strhash("D_PlayAnim");

void g_runAction(g_entity_t* self, char action[64], char args[64][7]);

// More complex actions that are externalized for readability.

#endif
